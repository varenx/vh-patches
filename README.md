VH patches for extending / altering chat functionality

currently only one patch available, allowing the user to mute posts based on
keywords.

To use the patches, copy-paste into console, then enter the command in the chat message box:

`/muteword politics`

OR

`/unmuteword politics`
