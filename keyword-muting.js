if (typeof window.vhChat === 'undefined') {
    window.vhChat = {
        plugins: [],
        events: [
            'onBeforeMessageRendered',
            'onBeforeMessageSend'
        ],
        handlers: {
            'onBeforeMessageRendered': [],
            'onBeforeMessageSend': []
        },
        getEventHandlers: function (eventName) {
            return this.handlers[eventName];
        },
        addHandler: function (eventName, handler) {
            this.handlers[eventName].push(handler);
        }
    }

}

if (typeof window.vhChat !== 'undefined') {
    if (typeof window.vhChat.plugins['mutekeyword'] === 'undefined') {
        window.vhChat.plugins['mutekeyword'] = {
            keywords: [],
            methods: {}
        };

        window.vhChat.plugins['mutekeyword'].methods.muteKeyword = function (keyword) {
            var index = window.vhChat
                .plugins['mutekeyword']
                .keywords
                .indexOf(keyword);
            if (index !== -1) {
                return;
            }
            window.vhChat.plugins['mutekeyword'].keywords.push(keyword);
        }

        window.vhChat.plugins['mutekeyword'].methods.unmuteKeyword = function (keyword) {
            var indexToSplice = window.vhChat
                .plugins['mutekeyword']
                .keywords
                .indexOf(keyword);

            if (indexToSplice !== -1) {
                window.vhChat.plugins['mutekeyword'].keywords.splice(
                    indexToSplice,
                    1
                );

            }
        }

        window.vhChat.addHandler('onBeforeMessageRendered', function (msg) {
            var i = 0;
            for (i = 0; i < window.vhChat.plugins['mutekeyword'].keywords.length; i += 1) {
                currentKeyword = window.vhChat.plugins['mutekeyword'].keywords[i];

                if (msg.body.match(new RegExp(currentKeyword, 'gim')) !== null) {
                    msg.body = msg.body.replace(/<span\s+class="spoiler">/gim, '');
                    msg.body = msg.body.replace(/<\/span>/gim, '');
                    msg.body = '<span class="spoiler">' +
                        msg.body + '</span> (Spoilered because of keyword: ' + currentKeyword + ' )'
                    break;
                }
            }
            return msg;
        });

        window.vhChat.addHandler('onBeforeMessageSend', function (msgBody) {
            var matches = [];

            // Should handle this before the message is sent to the server
            if ((matches = msgBody.match(/\/muteword (.*)/)) !== null) {
                window.vhChat.plugins['mutekeyword'].methods.muteKeyword(matches[1]);
                return true; // Prevent the message from sending
            }

            if ((matches = msgBody.match(/\/unmuteword (.*)/)) !== null) {
                window.vhChat.plugins['mutekeyword'].methods.unmuteKeyword(matches[1]);
                return true; // Prevent the message from sending
            }

            return false;
        });

    }
}

window.senderOnSubmit = function (event) {
    var form = this;
    var $bodyCtl = $(this.elements["body"]);
    var $msgTargetCtl = $(this.elements["msgTarget"]);
    var matches = null;
    var msg = $bodyCtl.val().trim();
    var handlersBeforeSend = [];
    var messageHandled = false;

    event.preventDefault();

    if (typeof window.vhChat !== 'undefined') {
        // Call all the handlers for the message
        handlersBeforeSend = window.vhChat.getEventHandlers('onBeforeMessageSend');
        if (handlersBeforeSend.length > 0) {
            for (i = 0; i < handlersBeforeSend.length; i += 1) {
                if (messageHandled === true) {
                    break;
                }
                messageHandled = (handlersBeforeSend[i](msg));

            }
            if (messageHandled === true) {
                // We shouldn't send a message, we got true back
                // meaning the handler already dealt with the message
                $bodyCtl.val('');
                return false;
            }
        }
    }

    // Handle browsers (Chrome) which don't block submit while disabled.
    if (form.disabled) {
        return;
    } else if (msg === "") {
        return;  // Don't bother proceeding if input box is empty.
    } else if ($bodyCtl.val()[0] !== "/" || handleChatCommands($bodyCtl.val(), form, $bodyCtl, $msgTargetCtl)) {
        sendMessage(form, $bodyCtl, $msgTargetCtl);
    }
};

window.handleChatMessage = function (msg) {
    var $msg = null;
    var i = 0;
    var currentKeyword = '';
    var handlersBeforeRender = [];

    if (typeof window.vhChat !== 'undefined') {
        // Call all the handlers for the message
        handlersBeforeRender = window.vhChat.getEventHandlers('onBeforeMessageRendered');
        if (handlersBeforeRender.length > 0) {
            for (i = 0; i < handlersBeforeRender.length; i += 1) {
                msg = handlersBeforeRender[i](msg);
            }
        }
    }

    $msg = renderChatMessage(msg);

    // Open hoverboxes automatically if settings say to do so.
    if (me.settings.autoHoverWhispers && msg.type === "WHISPER_CHAT") {
        if (msg.fromMe) {
            HoverBoxSystem({leash: msg.to.name, type: "whisper", title: msg.to.name}).create();
        } else {
            HoverBoxSystem({leash: msg.from.name, type: "whisper", title: msg.from.name}).create();
        }
    }
    else if (me.settings.autoHoverGroups && msg.type === "GROUP_CHAT") {
        HoverBoxSystem({leash: msg.toLeash, type: "groupchat", title: msg.to.name}).create();
    }

    HoverBoxSystem.routeToPane($msg);

    // Play sound if necessary
    if (msg.fromMe === false && msg.type === "WHISPER_CHAT") {
        if (me.settings.enableSound && sounds.whisper) sounds.whisper.play();
    }
    else if ($msg.hasClass("highlight")) {
        if (me.settings.enableSound && sounds.highlighted) sounds.highlighted.play();
    }
    else if ($msg.hasClass("nameding")) {
        if (me.settings.enableSound && sounds.nameding) sounds.nameding.play();
    }
    else if (msg.fromMe === false) {
        if (me.settings.enableSound && sounds.received) sounds.received.play();
    }

    // Add the title alert if window is not focused.
    if (window.isFocused === false
            && ($msg.hasClass("highlight") || $msg.hasClass("nameding"))
            && document.title.substr(0, TITLE_ALERT.length) !== TITLE_ALERT) {
        document.title = TITLE_ALERT + document.title;
    }
};
$("#main-sender-form").off("submit");
$("#main-sender-form").on("submit", window.senderOnSubmit);
$("#hoverbox-pane").off("submit", ".sender-form");
$("#hoverbox-pane").on("submit", ".sender-form", window.senderOnSubmit);
